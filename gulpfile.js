var gulp = require('gulp');
var taskListing = require('gulp-task-listing');
var requireDir = require('require-dir');

requireDir('gulp/tasks', { recurse: true });

gulp.task('default', taskListing);
gulp.task('help', taskListing);