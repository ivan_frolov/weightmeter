import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {LoginComponent} from "./login.component";
import {TasksComponent} from "./tasks.component";
import {CanActivateViaAuthGuard} from "./guards/can-activate.guard";

const appRoutes: Routes = [
	{ path: '', redirectTo: 'tasks', pathMatch: 'full' },
	{ path: 'login', component: LoginComponent },
	{
		path: 'tasks',
		component: TasksComponent,
		canActivate: [
			CanActivateViaAuthGuard
		]
	}
];

@NgModule({
	imports: [
		RouterModule.forRoot(appRoutes)
	],
	exports: [
		RouterModule
	],
	providers: [
		CanActivateViaAuthGuard
	]
})
export class AppRoutingModule {}