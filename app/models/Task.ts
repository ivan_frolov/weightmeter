export class Task {
	constructor(
		public name,
	    public description
	) {

	}

	public static build = (data: any): Task => {
		return new Task(
			data.Name,
			data.Description
		);
	}
}