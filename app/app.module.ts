import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule }    from '@angular/http';
import { FormsModule } from "@angular/forms";

import { UserService } from "./services/user.service"

import { AppComponent } from "./app.component";
import { TasksComponent } from "./tasks.component";
import { AppRoutingModule } from "./app-routing.module";
import { LoginComponent } from "./login.component";
import {TasksService} from "./services/tasks.service";



@NgModule({
	imports: [ 
		BrowserModule,
		AppRoutingModule,
		HttpModule,
		FormsModule
	],
	declarations: [
		AppComponent,
		LoginComponent,
		TasksComponent
	],
	providers: [
		UserService,
		TasksService
	],
	bootstrap: [AppComponent]
})

export class AppModule {

}