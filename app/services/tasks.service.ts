import { Injectable } from "@angular/core";
import {Http, Headers, RequestOptions} from "@angular/http";
import { AppConstants } from "../constants/AppConstants"
import 'rxjs/add/operator/toPromise';
import {Task} from "../models/Task";
import {UserService} from "./user.service";
import {Router} from "@angular/router";

@Injectable()
export class TasksService {

	constructor(
		private http: Http,
	    private userService: UserService,
	    private router: Router
	) {

	}

	public getTasks(): Promise<Task[]> {
		// we may create interceptor instead of this hack
		if (!this.checkIsLoggedIn()) {
			return Promise.reject<Task[]>("User is not logged in");
		}

		let method: string = "/api/todo";
		let headers: Headers = new Headers(
			{
				"Content-Type": "application/json",
				"Authorization": "Bearer " + window.localStorage.getItem("accessToken")
			}
		);
		let options: RequestOptions = new RequestOptions ({ headers: headers });

		return this.http.get(
			AppConstants.API_URL + method,
			options
		)
			.toPromise()
			.then((response) => {
					let responseBody = response.json();
					return Promise.resolve<Task[]>(responseBody.List.map(Task.build) as Task[]);
				}
			)
			.catch((response) => {
					return Promise.reject(response.json());
				}
			);

	};

	public createTask(task: Task): Promise<any> {
		// we may create interceptor instead of this hack
		if (!this.checkIsLoggedIn()) {
			return Promise.reject("User is not logged in");;
		}

		let method: string = "/api/todo";
		let headers: Headers = new Headers(
			{
				"Content-Type": "application/json",
				"Authorization": "Bearer " + window.localStorage.getItem("accessToken")
			}
		);
		let options: RequestOptions = new RequestOptions ({ headers: headers });
		let data: string = JSON.stringify(task);

		return this.http.post(
			AppConstants.API_URL + method,
			data,
			options
		)
			.toPromise()
			.then((response) => {
				response.json();
			})
			.catch((response) => {
				response.json();
			});

	}

	private checkIsLoggedIn(): boolean {
		let isLoggedIn: boolean = this.userService.isLoggedIn();
		if (!isLoggedIn) {
			this.router.navigate(['/login']);
		}

		return isLoggedIn;
	}
}
