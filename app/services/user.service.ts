import { Injectable } from "@angular/core";
import {Http, Headers, RequestOptions} from "@angular/http";
import { AppConstants } from "../constants/AppConstants"
import 'rxjs/add/operator/toPromise';
import {Observable} from "rxjs";
import {Router} from "@angular/router";

@Injectable()
export class UserService {

	constructor(
		private http: Http,
	    private router: Router
	) {

	}

	public requestedUrl = null;
	public logIn = (username: string, password: string): Promise<void> => {

		let method: string = "/token";
		let data: string = "grant_type=password&username="+username+"&password="+password;
		let headers: Headers = new Headers(
			{
				"Content-Type": "application/x-www-form-urlencoded",
				"Accept": "application/json, text/plain, */*"
			}
		);
		let options: RequestOptions = new RequestOptions ({ headers: headers });

		return this.http.post(
			AppConstants.API_URL + method,
			data,
			options
		)
			.toPromise()
			.then((response) => {
				window.localStorage.setItem("accessToken", response.json().access_token);
				if (this.requestedUrl !== null) {
					this.router.navigate([this.requestedUrl]);
				} else {
					this.router.navigate(['']);
				}
				this.requestedUrl = null;
			})
			.catch((response) => {
				response.json();
			});
	};

	public isLoggedIn = (): boolean => {
		return window.localStorage.hasOwnProperty("accessToken");
	}
}
