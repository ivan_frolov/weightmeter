import {Component, OnInit} from "@angular/core";
import { UserService } from "./services/user.service";
import {Router, RouterState} from "@angular/router";


export declare const __moduleName: string;


@Component({
	moduleId: __moduleName,
	selector: "login",
	templateUrl: "login.component.html"
})

export class LoginComponent implements OnInit{

	constructor(
		private userService: UserService,
	    private router: Router
	) {
		const state: RouterState = router.routerState;
		debugger;
	}

	ngOnInit(): void {

	}

	public username: string = "";
	public password: string = "";

	public logIn = (event): void => {
		event.preventDefault();
		this.userService.logIn(this.username, this.password);
	}

}