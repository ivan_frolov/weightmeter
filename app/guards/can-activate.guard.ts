import { Injectable } from '@angular/core';
import {CanActivate, Router, ActivatedRoute, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {UserService} from "../services/user.service";

@Injectable()
export class CanActivateViaAuthGuard implements CanActivate {

	constructor(private userService: UserService, private router: Router) {}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		this.userService.requestedUrl = state.url;
		if (!this.userService.isLoggedIn()) {
			debugger;
			this.router.navigate(['/login']);
		}
		return this.userService.isLoggedIn();
	}


}