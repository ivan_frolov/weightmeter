import {Component, OnInit} from "@angular/core";
import {TasksService} from "./services/tasks.service";
import {Task} from "./models/Task";


export declare const __moduleName: string;


@Component({
	moduleId: __moduleName,
	selector: "tasks",
	templateUrl: "tasks.component.html"
})

export class TasksComponent implements OnInit{

	public tasks: Task[] = [];
	public newTaskName: string = "";
	public newTaskDescription: string = "";
	public errorMessages: string[] = [];
	public isProcessing: boolean = false;

	constructor(
		private tasksService: TasksService
	) {

	}

	ngOnInit(): void {
		this.getTasks();
	}

	public createTask = (): void => {
		this.errorMessages = [];

		if (!!!this.newTaskName.length) {
			this.errorMessages.push("Task name is required");
		}
		if (!!!this.newTaskDescription.length) {
			this.errorMessages.push("Task description is required");
		}
		
		if (this.errorMessages.length === 0) {
			this.isProcessing = true;
			this.tasksService.createTask(new Task(this.newTaskName, this.newTaskDescription))
				.then(result => {
					this.isProcessing = false;
					this.newTaskName = "";
					this.newTaskDescription = "";

					this.getTasks();
					console.log(result);
				})
		}

	}

	private getTasks = () => {
		this.tasksService.getTasks()
			.then(result => {
				this.tasks = result;
			})
	}


}