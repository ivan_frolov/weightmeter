var gulp = require("gulp");
var concat = require("gulp-concat");
var config = require("../config.json");

var sourceJsVendorsFiles = [
	"node_modules/core-js/client/shim.min.js",
	"node_modules/zone.js/dist/zone.js",
	"node_modules/reflect-metadata/Reflect.js",
	"node_modules/systemjs/dist/system.src.js",
	'node_modules/@angular/core/bundles/core.umd.js',
    'node_modules/@angular/common/bundles/common.umd.js',
    'node_modules/@angular/compiler/bundles/compiler.umd.js',
    'node_modules/@angular/platform-browser/bundles/platform-browser.umd.js',
    'node_modules/@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
    'node_modules/@angular/http/bundles/http.umd.js',
    'node_modules/@angular/router/bundles/router.umd.js',
    'node_modules/@angular/forms/bundles/forms.umd.js',
    'node_modules/@angular/upgrade/bundles/upgrade.umd.js',
    'node_modules/rxjs/**',
	'node_modules/angular-in-memory-web-api/bundles/in-memory-web-api.umd.js',
    'node_modules/bootstrap/dist/css/bootstrap.min.css',
    'node_modules/bootstrap/dist/js/bootstrap.min.js',
    'node_modules/bootstrap/dist/fonts/glyphicons-halflings-regular.eot',
    'node_modules/bootstrap/dist/fonts/glyphicons-halflings-regular.svg',
    'node_modules/bootstrap/dist/fonts/glyphicons-halflings-regular.ttf',
    'node_modules/bootstrap/dist/fonts/glyphicons-halflings-regular.woff',
    'node_modules/bootstrap/dist/fonts/glyphicons-halflings-regular.woff2'
]

var destinationJsFile = "../../php/weightmeter/assets/vendor";

// gulp.task("concatenate-js", function () {
//     return gulp.src(sourceJsVendorsFiles, { base: 'node_modules/' })
//         .pipe(concat('vendorscripts.js'))
//         .pipe(gulp.dest(destinationJsFile));
// });

gulp.task("inject-vendor-scripts", function() {
	return gulp.src(sourceJsVendorsFiles, { base: 'node_modules/' })
	.pipe(gulp.dest(destinationJsFile))
})