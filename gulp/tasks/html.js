var gulp = require("gulp");

var serverPath = "../../php/weightmeter/app";

gulp.task("html", function () {
    return gulp.src('app/**/*.html')
        	.pipe(gulp.dest(serverPath));
});