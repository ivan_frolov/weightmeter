var gulp = require("gulp");
var del = require("del");

var serverPath = "../../php/weightmeter";
var appPath = serverPath + "/app";
var assetsPath = serverPath + "/assets";

gulp.task("clean-all", function () {
    return del([appPath, assetsPath], {force: true})
            .then((paths) => {
    	        console.log('Deleted files and folders:\n', paths.join('\n'));
	        });
});

gulp.task("clean-sources", function () {
    return del(appPath, {force: true})
        .then((paths) => {
            console.log('Deleted files and folders:\n', paths.join('\n'));
        });
});