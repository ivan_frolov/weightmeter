var gulp = require("gulp");
var runSequence = require("run-sequence");

gulp.task("build", function (callback) {
    runSequence('clean-all', 'ts', ['inject-vendor-scripts', 'html'], callback);
});

gulp.task("rebuild", function (callback) {
    runSequence('clean-sources', 'ts', 'html', callback);
});